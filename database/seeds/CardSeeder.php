<?php

use Illuminate\Database\Seeder;
use App\Card;
use Carbon\Carbon;
class CardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('cards')->truncate();
         $currentDate = Carbon::now('America/Montevideo');

         Card::create([
         	'name'=> 'Wizard',
         	'description'=>'Remove one wrong answer',
         	'picture' => 'https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1048941/300/200/m1/fpnw/wm0/wizard-hat_-.jpg?1456997011&s=63434a413546b077bad91ef3ccf41a04',
         	'price' => 2450,
         	'created_at' => $currentDate,
         	'updated_at' => $currentDate
         	]);

         Card::create([
         	'name'=> 'Next One',
         	'description'=>'Pass to next question',
         	'picture' => 'http://www.pokegravy.com/images/comics_next.png',
         	'price' => 1250,
         	'created_at' => $currentDate,
         	'updated_at' => $currentDate
         	]);

         Card::create([
         	'name'=> 'Life',
         	'description'=>'This card give you an extra life',
         	'picture' => 'http://www.118healthandsafety.co.uk/s/cc_images/cache_25231972.jpg?t=1388698141',
         	'price' => 3750,
         	'created_at' => $currentDate,
         	'updated_at' => $currentDate
         	]);
    }
}
