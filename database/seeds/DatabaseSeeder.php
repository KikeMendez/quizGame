<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @IMPORTANT:  
         * First we truncate the table
         *
         * 
         * The seeds will be inserted just in the same order as you see it here.
         * Carefull with the question_id in the answers seeders when you make the connection
         * Between question and answers. 
        */
        $this->call(TruncateQuestion::class);  // Keep this line here.
    	$this->call(HistoryQuestionSeeder::class); // Keep this line here.
        $this->call(SportQuestionSeeder::class); // Keep this line here.
        /**
         * Answers Seeder  
        */
        $this->call(TruncateAnswer::class); // Keep this line here.
        $this->call(HistoryAnswerSeeder::class);// Keep this line here.
        $this->call(SportAnswerSeeder::class);  // Keep this line here.
        
        /**
         * User Seeder  
        */
        $this->call(UserSeeder::class);

        /**
         * Cards  
        */
        $this->call(CardSeeder::class);

        /**
         * User kit Seeder  
        */
        $this->call(UsersKitSeeder::class);

        /**
         * Level Seeder  
        */
        $this->call(LevelSeeder::class);
        
        /**
         * Category Seeder  
        */    
        $this->call(CategorySeeder::class);
    }
}
