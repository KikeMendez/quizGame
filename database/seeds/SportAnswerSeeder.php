<?php

use Illuminate\Database\Seeder;
use App\Answer;
use Carbon\Carbon;
class SportAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $currentDate = Carbon::now('America/Montevideo');

      /*51  Which sport does Constantino Rocca play 
      */ 
      Answer::create([
      'id_question' => 51,
      'answer' => 'Golf',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*51*/ Answer::create([
      'id_question' => 51,
      'answer' => 'Rugby',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*51*/ Answer::create([
      'id_question' => 51,
      'answer' => 'Tennis',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*52 Name the only heavyweight boxing champion to finish his career of 49 fights without ever having been defeated? 
      */ 
      Answer::create([
      'id_question' => 52,
      'answer' => ' Rocky Marciano',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*52*/ Answer::create([
      'id_question' => 52,
      'answer' => 'Rocky Balboa',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*52*/ Answer::create([
      'id_question' => 52,
      'answer' => 'Mike tyson',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*53 Name the British gymnast who won the men's floor exercise and pommel horse at the 2016 Olympics? 
      */ 
      Answer::create([
      'id_question' => 53,
      'answer' => 'Max Whitlock',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*53*/ Answer::create([
      'id_question' => 53,
      'answer' => 'Amy Tinkler',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*53*/ Answer::create([
      'id_question' => 53,
      'answer' => 'Jennifer Pinches',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*54 Which Asian country came fifth in the 2012 London Olympics Medal Table 
      */ 
      Answer::create([
      'id_question' => 54,
      'answer' => 'South Korea',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*54*/ Answer::create([
      'id_question' => 54,
      'answer' => 'North Korea',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*54*/ Answer::create([
      'id_question' => 54,
      'answer' => 'China',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*55 Which club, founded in 1987, are the most successful in English women's football?
      */ 
      Answer::create([
      'id_question' => 55,
      'answer' => 'Arsenal',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*55*/ Answer::create([
      'id_question' => 55,
      'answer' => 'Manchester City',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*55*/ Answer::create([
      'id_question' => 55,
      'answer' => 'Liverpool',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*56 Which Scandinavian town hosted the 1994 Winter Olympics
      */ 
      Answer::create([
      'id_question' => 56,
      'answer' => 'Lillehammer',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*56*/ Answer::create([
      'id_question' => 56,
      'answer' => 'Oslo',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*56*/ Answer::create([
      'id_question' => 56,
      'answer' => 'Grimstad',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*57 Which sport's ball size was changed from 38mmm to 40mm in 2000 to slow down the game?
      */ 
      Answer::create([
      'id_question' => 57,
      'answer' => 'Table tennis',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*57*/ Answer::create([
      'id_question' => 57,
      'answer' => 'Golf',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*57*/ Answer::create([
      'id_question' => 57,
      'answer' => 'Baseball',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*58 Who holds the club goal scoring record for Tottenham Hotspur?
      */ 
      Answer::create([
      'id_question' => 58,
      'answer' => 'Jimmy Greaves',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*58*/ Answer::create([
      'id_question' => 58,
      'answer' => 'Bobby Smith',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*58*/ Answer::create([
      'id_question' => 58,
      'answer' => 'Robbie Keane',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*59 The Sochi Autodrom is home to which country's Grand Prix?
      */ 
      Answer::create([
      'id_question' => 59,
      'answer' => 'Russia',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*59*/ Answer::create([
      'id_question' => 59,
      'answer' => 'Japan',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*59*/ Answer::create([
      'id_question' => 59,
      'answer' => 'China',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*60 Who won The World Highland Games Championships a record six times?
      */ 
      Answer::create([
      'id_question' => 60,
      'answer' => 'Geoff Capes',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*60*/ Answer::create([
      'id_question' => 60,
      'answer' => 'Jim McGoldrick',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*60*/ Answer::create([
      'id_question' => 60,
      'answer' => 'Josh Delzell',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*61 Which former rugby player once called the English RFU committee 'Old Farts'?'
      */ 
      Answer::create([
      'id_question' => 61,
      'answer' => 'Will Carling',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*61*/ Answer::create([
      'id_question' => 61,
      'answer' => 'Anthony Watson',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*61*/ Answer::create([
      'id_question' => 61,
      'answer' => 'John Kirwan',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*62 Frankie Fredericks represented which African country in athletics?
      */ 
      Answer::create([
      'id_question' => 62,
      'answer' => 'Namibia',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*62*/ Answer::create([
      'id_question' => 62,
      'answer' => 'Morroco',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*62*/ Answer::create([
      'id_question' => 62,
      'answer' => 'Kenya',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*63 In inches, how big is the diameter of a basketball hoop?
      */ 
      Answer::create([
      'id_question' => 63,
      'answer' => '18',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*63*/ Answer::create([
      'id_question' => 63,
      'answer' => '22',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*63*/ Answer::create([
      'id_question' => 63,
      'answer' => '20',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*64  Wladimir Klitschko is a champion boxer from which country
      */ 
      Answer::create([
      'id_question' => 64,
      'answer' => 'Ukraine',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*64*/ Answer::create([
      'id_question' => 64,
      'answer' => 'Russia',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*64*/ Answer::create([
      'id_question' => 64,
      'answer' => 'Czech Republic',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*65  In British's horse racing, which is the last of the five classics to be run each year, and its distance is also the longest
      */ 
      Answer::create([
      'id_question' => 65,
      'answer' => 'St Leger',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*65*/ Answer::create([
      'id_question' => 65,
      'answer' => 'Derby',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*65*/ Answer::create([
      'id_question' => 65,
      'answer' => 'One Thousand Guineas',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*66 In golf, who won won his first major at the Masters in 2015 and also won the 2015 U.S. Open
      */ 
      Answer::create([
      'id_question' => 66,
      'answer' => 'Jordan Spieth',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*66*/ Answer::create([
      'id_question' => 66,
      'answer' => 'Tiger Woods',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*66*/ Answer::create([
      'id_question' => 66,
      'answer' => 'Dustin Johnson',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*67 In feet, how high is a basketball hoop
      */ 
      Answer::create([
      'id_question' => 67,
      'answer' => '10 feet',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*67*/ Answer::create([
      'id_question' => 67,
      'answer' => '12 feet',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*67*/ Answer::create([
      'id_question' => 67,
      'answer' => '9 feet',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*68 What nationality is tennis player Novak Djokovic
      */ 
      Answer::create([
      'id_question' => 68,
      'answer' => 'Serbian',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*68*/ Answer::create([
      'id_question' => 68,
      'answer' => 'Swedish',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*68*/ Answer::create([
      'id_question' => 68,
      'answer' => 'Norwegian',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*69 What nationality is tennis player Rafael Nadal
      */ 
      Answer::create([
      'id_question' => 69,
      'answer' => 'Spanish',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*69*/ Answer::create([
      'id_question' => 69,
      'answer' => 'Argentinean',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*69*/ Answer::create([
      'id_question' => 69,
      'answer' => 'Colombian',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*70 What nationality is the football player Luis Suarez
      */ 
      Answer::create([
      'id_question' => 70,
      'answer' => 'Uruguayan',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*70*/ Answer::create([
      'id_question' => 70,
      'answer' => 'Argentinean',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*70*/ Answer::create([
      'id_question' => 70,
      'answer' => 'Spanish',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*71 Jan-Ove Waldner, Zhang Jike and Deng Yaping have been noteable players in which sport
      */ 
      Answer::create([
      'id_question' => 71,
      'answer' => 'Table Tennis',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*71*/ Answer::create([
      'id_question' => 71,
      'answer' => 'Taekwondo',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*71*/ Answer::create([
      'id_question' => 71,
      'answer' => 'Wrestling',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*72 For which county does cricketer Joe Root play
      */ 
      Answer::create([
      'id_question' => 72,
      'answer' => 'Yorkshire',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*72*/ Answer::create([
      'id_question' => 72,
      'answer' => 'Somerset',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*72*/ Answer::create([
      'id_question' => 72,
      'answer' => 'Gloucestershire',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*73 Which male British Paralympic wheelchair athlete has won the London Marathon six times
      */ 
      Answer::create([
      'id_question' => 73,
      'answer' => 'David Weir',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*73*/ Answer::create([
      'id_question' => 73,
      'answer' => 'Anthony Kappes',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*73*/ Answer::create([
      'id_question' => 73,
      'answer' => 'Brian Johnson',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*74 Who won the 2013 Tour de France
      */ 
      Answer::create([
      'id_question' => 74,
      'answer' => 'Chris Froome',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*74*/ Answer::create([
      'id_question' => 74,
      'answer' => 'Vincenzo Nibali',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*74*/ Answer::create([
      'id_question' => 74,
      'answer' => 'Alberto Contador',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);


      /*75 Who won the 2015 Tour de France
      */ 
      Answer::create([
      'id_question' => 75,
      'answer' => 'Chris Froome',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*75*/ Answer::create([
      'id_question' => 75,
      'answer' => 'Bradley Wiggins',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*75*/ Answer::create([
      'id_question' => 75,
      'answer' => 'Vincenzo Nibali',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*76 Who won the 2015 Tour de France
      */ 
      Answer::create([
      'id_question' => 76,
      'answer' => 'Chris Froome',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*76*/ Answer::create([
      'id_question' => 76,
      'answer' => 'Anthony Kappes',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*76*/ Answer::create([
      'id_question' => 76,
      'answer' => 'Brian Johnson',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*77 The Borg-Warner Trophy is awarded to the winner of which race
      */ 
      Answer::create([
      'id_question' => 77,
      'answer' => 'Indianapolis 500',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*77*/ Answer::create([
      'id_question' => 77,
      'answer' => '24 hours le mans',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*77*/ Answer::create([
      'id_question' => 77,
      'answer' => 'Australian Sports Car Championship',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);


      /*78 How many players are on the field for each team in an Australian rules football match
      */ 
      Answer::create([
      'id_question' => 78,
      'answer' => '18',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*78*/ Answer::create([
      'id_question' => 78,
      'answer' => '15',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*78*/ Answer::create([
      'id_question' => 78,
      'answer' => '12',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);


      /*79 Which club did rugby legend Martin Johnson play for
      */ 
      Answer::create([
      'id_question' => 79,
      'answer' => 'Leicester Tigers',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*79*/ Answer::create([
      'id_question' => 79,
      'answer' => 'Exeter Chiefs',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*79*/ Answer::create([
      'id_question' => 79,
      'answer' => 'Bath Rugby',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*80 The American basketball team 'The Bulls', represent which city
      */ 
      Answer::create([
      'id_question' => 80,
      'answer' => 'Chicago',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*80*/ Answer::create([
      'id_question' => 80,
      'answer' => 'Detroit',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*80*/ Answer::create([
      'id_question' => 80,
      'answer' => 'Miami',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*81 Name the European club David Beckham ended his career with
      */ 
      Answer::create([
      'id_question' => 81,
      'answer' => 'Paris Saint-Germain',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*81*/ Answer::create([
      'id_question' => 81,
      'answer' => 'Real Madrid',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*81*/ Answer::create([
      'id_question' => 81,
      'answer' => 'Milan',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*82 Which sport is Silverstone most associated with
      */ 
      Answer::create([
      'id_question' => 82,
      'answer' => 'Motor Racing',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*82*/ Answer::create([
      'id_question' => 82,
      'answer' => 'Surf',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*82*/ Answer::create([
      'id_question' => 82,
      'answer' => 'Football',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*83 How many Fifa World Cup have Uruguay?
      */ 
      Answer::create([
      'id_question' => 83,
      'answer' => 'Two',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*83*/ Answer::create([
      'id_question' => 83,
      'answer' => 'Four',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*83*/ Answer::create([
      'id_question' => 83,
      'answer' => 'Zero',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);    

      /*84 How many world championships won michael schumacher
      */ 
      Answer::create([
      'id_question' => 84,
      'answer' => 'Seven',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*84*/ Answer::create([
      'id_question' => 84,
      'answer' => 'Four',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*84*/ Answer::create([
      'id_question' => 84,
      'answer' => 'Ten',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);              


      /*85 Who won fifa world cup 2002?
      */ 
      Answer::create([
      'id_question' => 85,
      'answer' => 'Brazil',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*85*/ Answer::create([
      'id_question' => 85,
      'answer' => 'Germany',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*85*/ Answer::create([
      'id_question' => 85,
      'answer' => 'Spain',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*86 Who was the youngest player in the NBA
      */ 
      Answer::create([
      'id_question' => 86,
      'answer' => 'Andrew Bynum',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*86*/ Answer::create([
      'id_question' => 86,
      'answer' => 'Michael Jordan',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*86*/ Answer::create([
      'id_question' => 86,
      'answer' => 'Kobe Bean Bryant',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);              


      /*87 How much was lebron James paid in 2015
      */ 
      Answer::create([
      'id_question' => 87,
      'answer' => '64.6 million USD',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*87*/ Answer::create([
      'id_question' => 87,
      'answer' => '30.96 million USD',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*87*/ Answer::create([
      'id_question' => 87,
      'answer' => '12.96 million USD',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]); 

      /*88 Adolf Dassler founded which sports company in 1948
      */ 
      Answer::create([
      'id_question' => 88,
      'answer' => 'Adidas',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*88*/ Answer::create([
      'id_question' => 88,
      'answer' => 'Nike',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*88*/ Answer::create([
      'id_question' => 88,
      'answer' => 'Puma',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*89 Which famous footballer was named: ‘Athlete of the Century’ by the International Olympic Committee in 1999
      */ 
      Answer::create([
      'id_question' => 89,
      'answer' => 'Edson Arantes do Nascimento',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*89*/ Answer::create([
      'id_question' => 89,
      'answer' => 'Diego Maradona',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*89*/ Answer::create([
      'id_question' => 89,
      'answer' => 'Franz Beckenbauer',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);             


      /*90 Moving clockwise around a dartboard what comes next after 6
      ```*/ 
      Answer::create([
      'id_question' => 90,
      'answer' => 'Ten',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*90*/ Answer::create([
      'id_question' => 90,
      'answer' => 'Nine',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*90*/ Answer::create([
      'id_question' => 90,
      'answer' => 'Twelve',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*91 In which sport did Aussie duo Kerri Pottharst and Natalie Cook win gold at the 2000 Olympic Games
      ```*/ 
      Answer::create([
      'id_question' => 91,
      'answer' => 'Beach Volleyball',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*91*/ Answer::create([
      'id_question' => 91,
      'answer' => 'HandBall',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*91*/ Answer::create([
      'id_question' => 91,
      'answer' => 'Table Tennis',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]); 

      /*92 In 2011, which country hosted a Formula 1 race for the first time
      ```*/ 
      Answer::create([
      'id_question' => 92,
      'answer' => 'India',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*92*/ Answer::create([
      'id_question' => 92,
      'answer' => 'Argentina',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*92*/ Answer::create([
      'id_question' => 92,
      'answer' => 'Mexico',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);             

      /*93 Name the only footballer to have played for Liverpool, Everton, Manchester City and Manchester United
      ```*/ 
      Answer::create([
      'id_question' => 93,
      'answer' => 'Peter Beardsley.',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*93*/ Answer::create([
      'id_question' => 93,
      'answer' => 'Peter Crouch.',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*93*/ Answer::create([
      'id_question' => 93,
      'answer' => 'Paul Scholes',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);   


      /*94 Which was the first tennis Grand Slam event to introduce the innovative Hawk-Eye technology
      */ 
      Answer::create([
      'id_question' => 94,
      'answer' => 'U.S Open',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*94*/ Answer::create([
      'id_question' => 94,
      'answer' => 'wimbledon',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*94*/ Answer::create([
      'id_question' => 94,
      'answer' => 'Roland Garros',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*95 Which of the martial arts became an official Olympic sport at the 2000 Sydney Olympic games
      */ 
      Answer::create([
      'id_question' => 95,
      'answer' => 'Taekwondo',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*95*/ Answer::create([
      'id_question' => 95,
      'answer' => 'Karate',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*95*/ Answer::create([
      'id_question' => 95,
      'answer' => 'Aikido',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*96 what it was the stadium's first game of the World Cup 1930
      */ 
      Answer::create([
      'id_question' => 96,
      'answer' => 'El Gran Parque Central',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*96*/ Answer::create([
      'id_question' => 96,
      'answer' => 'Estadio Centenario',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*96*/ Answer::create([
      'id_question' => 96,
      'answer' => 'Campeon Del Siglo',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*97 In which athletics event did Britain’s Katharine Merry win a bronze medal at the 2000 Sydney Olympics
      */ 
      Answer::create([
      'id_question' => 97,
      'answer' => '400 Meters',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*97*/ Answer::create([
      'id_question' => 97,
      'answer' => '200 Meters',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*97*/ Answer::create([
      'id_question' => 97,
      'answer' => 'Javelin throw',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*98 Name the country where you would find the Cresta Run
      */ 
      Answer::create([
      'id_question' => 98,
      'answer' => 'Switzerland',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*98*/ Answer::create([
      'id_question' => 98,
      'answer' => 'Austria',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*98*/ Answer::create([
      'id_question' => 98,
      'answer' => 'Germany',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*99 In football, who was nicknamed 'The Divine Ponytail'?
      */ 
      Answer::create([
      'id_question' => 99,
      'answer' => 'Roberto Baggio',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*99*/ Answer::create([
      'id_question' => 99,
      'answer' => 'Hernan Batistuta',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*99*/ Answer::create([
      'id_question' => 99,
      'answer' => 'Fernando Hierro',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*100 How many times was the Men's Tennis Singles at Wimbledon won by Bjorn Borg?
      */ 
      Answer::create([
      'id_question' => 100,
      'answer' => ' Five',
      'isCorrect' => 1,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*100*/ Answer::create([
      'id_question' => 100,
      'answer' => 'Two',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*100*/ Answer::create([
      'id_question' => 100,
      'answer' => 'Zero',
      'isCorrect' => 0,
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);
    }
}
