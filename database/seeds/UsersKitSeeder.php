<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\UsersKit;
class UsersKitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {		
    	DB::table('users_kits')->truncate();
    	$currentDate = Carbon::now('America/Montevideo');

        UsersKit::create([
        	'user_id' =>1,
        	'gems' =>1000,
        	'created_at' =>	$currentDate,
        	'updated_at' => $currentDate
        	]);

        UsersKit::create([
        	'user_id' =>2,
        	'gems' =>2000,
        	'created_at' =>	$currentDate,
        	'updated_at' => $currentDate
        	]);
    }
}
