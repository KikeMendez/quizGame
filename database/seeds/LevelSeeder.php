<?php

use Illuminate\Database\Seeder;
use App\Level;
use Carbon\Carbon;
class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {		
    	// First truncate the table
      DB::table('levels')->truncate();
      $currentDate = Carbon::now('America/Montevideo');

      Level::create([
      'name' => 'baby',
      'gems'	=>	200,
      'avatar' => '',
      'created_at' => $currentDate,
      'updated_at' => $currentDate 
      ]);

      Level::create([
      'name' => 'child',
      'gems'	=>	500,
      'avatar' => '',
      'created_at' => $currentDate,
      'updated_at' => $currentDate 
      ]);

      Level::create([
      'name' => 'teen',
      'gems'  =>  1500,
      'avatar' => '',
      'created_at' => $currentDate,
      'updated_at' => $currentDate 
      ]);

      Level::create([
      'name' => 'adult',
      'gems'  =>  2500,
      'avatar' => '',
      'created_at' => $currentDate,
      'updated_at' => $currentDate 
      ]);
    }
}
