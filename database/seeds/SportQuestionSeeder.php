<?php

use Illuminate\Database\Seeder;
use App\Question;
use Carbon\Carbon;

class SportQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * All the sports question are here.
     *
     * @return void
     */
    public function run()
    {
      
        // get the current date
        $currentDate = Carbon::now('America/Montevideo');

        /*51*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which sport does Constantino Rocca play',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*52*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Name the only heavyweight boxing champion to finish his career of 49 fights without ever having been defeated',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*53*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Name the British gymnast who won the men\'s floor exercise and pommel horse at the 2016 Olympics',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*54*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which Asian country came fifth in the 2012 London Olympics Medal Table',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*55*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which club, founded in 1987, are the most successful in English women\'s football?',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*56*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which Scandinavian town hosted the 1994 Winter Olympics',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*57*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which sport\'s ball size was changed from 38mmm to 40mm in 2000 to slow down the game',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*58*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Who holds the club goal scoring record for Tottenham Hotspur',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*59*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'The Sochi Autodrom is home to which country\'s Grand Prix',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*60*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Who won The World Highland Games Championships a record six times',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*61*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which former rugby player once called the English RFU committee \'Old Farts\'?',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*62*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Frankie Fredericks represented which African country in athletics',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*63*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'In inches, how big is the diameter of a basketball hoop',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*64*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Wladimir Klitschko is a champion boxer from which country',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*65*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'In British\'s horse racing, which is the last of the five classics to be run each year, and its distance is also the longest',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*66*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'In golf, who won won his first major at the Masters in 2015 and also won the 2015 U.S. Open',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*67*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'In feet, how high is a basketball hoop',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*68*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'What nationality is the tennis player Novak Djokovic',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*69*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'What nationality is the tennis player Rafael Nadal',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*70*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'What nationality is the football player Luis Suarez',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*71*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Jan-Ove Waldner, Zhang Jike and Deng Yaping have been noteable players in which sport',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*72*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'For which county does cricketer Joe Root play',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*73*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which male British Paralympic wheelchair athlete has won the London Marathon six times',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*74*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Who won the 2013 Tour de France',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*75*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Who won the 2015 Tour de France',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*76*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Who won the 2016 Tour de France',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*77*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'The Borg-Warner Trophy is awarded to the winner of which race',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*78*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'How many players are on the field for each team in an Australian rules football match',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*79*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which club did rugby legend Martin Johnson play for',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*80*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'The American basketball team \'The Bulls\', represent which city',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*81*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Name the European club David Beckham ended his career with',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*82*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which sport is Silverstone most associated with',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*83*/ Question::create([
        'id_level' => 1,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'How many Fifa World Cup have Uruguay',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*84*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'How many world championships won michael schumacher',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*85*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Who won fifa world cup 2002',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*86*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Who was the youngest player in the NBA',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*87*/ Question::create([
        'id_level' => 2,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'How much was Lebron James paid in 2015',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*88*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Adolf Dassler founded which sports company in 1948',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*89*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which famous footballer was named: ‘Athlete of the Century’ by the International Olympic Committee in 1999',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*90*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Moving clockwise around a dartboard what comes next after 6',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*91*/ Question::create([
        'id_level' => 3,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'In which sport did Aussie duo Kerri Pottharst and Natalie Cook win gold at the 2000 Olympic Games',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);


        /*92*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'In 2011, which country hosted a Formula 1 race for the first time',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);


        /*93*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Name the only footballer to have played for Liverpool, Everton, Manchester City and Manchester United',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*94*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which was the first tennis Grand Slam event to introduce the innovative Hawk-Eye technology',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*95*/ Question::create([
        'id_level' => 4,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Which of the martial arts became an official Olympic sport at the 2000 Sydney Olympic games',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*96*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'what it was the stadium\'s first game of the World Cup 1930',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*97*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'In which athletics event did Britain’s Katharine Merry win a bronze medal at the 2000 Sydney Olympics',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*98*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'Name the country where you would find the Cresta Run',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);


        /*99*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'In football, who was nicknamed \'The Divine Ponytail\'?',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);

        /*100*/ Question::create([
        'id_level' => 5,
        'id_category' => 1,
        'type_of_question' => 'multiOption',
        'question' => 'How many times was the Men\'s Tennis Singles at Wimbledon won by Bjorn Borg',
        'gems' => 10,
        'published' => true,
        'created_at' => $currentDate,
        'updated_at' => $currentDate
        ]);
    }
}

