<?php

use Illuminate\Database\Seeder;
use App\Question;
use Carbon\Carbon;
class HistoryQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * Here are all the history questions 	
     *
     * @return void
     */
    public function run()
    {
      // get the current date
      $currentDate = Carbon::now('America/Montevideo');

      /*1*/ Question::create([
           	'id_level' => 1,
           	'id_category' => 4,
            'type_of_question' => 'multiOption',
           	'question' => 'When did the Cold War end',
           	'gems' => 10,
           	'published' => true,
           	'created_at' => $currentDate,
           	'updated_at' => $currentDate
           	]);

      /*2*/ Question::create([
             'id_level' => 1,
             'id_category' => 4,
             'type_of_question' => 'multiOption',
             'question' => 'When did Margaret Thatcher become Prime Minister',
             'gems' => 10,
             'published' => true,
             'created_at' => $currentDate,
             'updated_at' => $currentDate
             ]);

      /*3*/ Question::create([
             'id_level' => 2,
             'id_category' => 4,
             'type_of_question' => 'multiOption',
             'question' => 'Which famous battle between the British Royal Navy and the combined fleets of the French Navy and Spanish Navy took place on 21st October 1805',
             'gems' => 15,
             'published' => true,
             'created_at' => $currentDate,
             'updated_at' => $currentDate
             ]);

      /*4*/ Question::create([
             'id_level' => 2,
             'id_category' => 4,
             'type_of_question' => 'multiOption',
             'question' => 'When was the euro introduced as legal currency on the world market',
             'gems' => 15,
             'published' => true,
             'created_at' => $currentDate,
             'updated_at' => $currentDate
             ]);

      /*5*/ Question::create([
             'id_level' => 3,
             'id_category' => 4,
             'type_of_question' => 'multiOption',
             'question' => 'Who was Henry VIII\'s first wife',
             'gems' => 25,
             'published' => true,
             'created_at' => $currentDate,
             'updated_at' => $currentDate
             ]);

      /*6*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'When was William Shakespeare born',
           'gems' => 25,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

       /*7*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'On what date did the Battle of Culloden take place',
           'gems' => 30,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

       /*8*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Who was the architect who designed the Millennium Dome',
           'gems' => 20,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*9*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'When did the Eurostar train service between Britain and France start running',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

         /*10*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'How many wives did King Henry VIII have',
           'gems' => 5,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*11*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'What year was the Battle of Hastings',
           'gems' => 5,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*12*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'What was the name of the German leader during World War II',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*13*/ Question::create([
           'id_level' => 2,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'In which year did the Victorian era begin',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*14*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Robert Curthose, who died in Cardiff Castle in his early eighties, was the eldest son of which king',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*15*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'What did Mary I lose in January 1558',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*16*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which battle of 1571 marked the end of the Ottoman naval supremacy in the Mediterranean',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*17*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'What tax was introduced in England and Wales in 1696 and repealed in 1851',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*18*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'which was the most famous battle of 1342',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*19*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which book was published in London on April 25th, 1719',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*20*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'The first day of which battle was the worst day in the history of the British Army, which suffered 60,000 casualties',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*21*/ Question::create([
           'id_level' => 2,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Who became president after the assassination of Abraham Lincoln',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*22*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which city is normally accepted as being the ancient capital of Wessex',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*23*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'In which year was the death of Queen Elizabeth I',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

        /*24*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'In 1297, at which battle did William Wallace defeat the English',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

         /*25*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Name the second largest city in Britain during the Black Death',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*26*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'In which war was The Battle of Agincourt',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*27*/ Question::create([
           'id_level' => 2,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'What nationality was Karl Marx',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*28*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Who commanded the British Expeditionary Force in World War One',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*29*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which country did Britain fight in the War of Jenkins\'s Ear',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*30*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'What was the largest naval battle of the First World War',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

           /*31*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'In which year was Abraham Lincoln assassinated',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

           /*32*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Name the second wife of Henry VIII',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

           /*33*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'In which military campaign did Lewis Halliday and Basil Guy win Victoria Crosses in 1900',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);


           /*34*/ Question::create([
           'id_level' => 2,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Who led a band of abolitionists that seized the federal arsenal at Harpers Ferry, Virginia, in October 1859',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

           /*35*/ Question::create([
           'id_level' => 5,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which English philosopher, regarded as one of the most influential of Enlightenment thinkers, was known as the Father of Classical Liberalism',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

           /*36*/ Question::create([
           'id_level' => 5,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which German city endured the worst bombing of World War II in February 1945',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

           /*37*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Robert Curthose was the son of which king',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

           /*38*/ Question::create([
           'id_level' => 2,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Who was king before Queen Victoria',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

           /*39*/ Question::create([
           'id_level' => 5,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which Empire declared war on Russia in October 1853, suffering a defeat that gave Russia control of the Black Sea',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*40*/ Question::create([
           'id_level' => 4,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which Roman road stretched 220 miles from Exeter to Lincoln',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*41*/ Question::create([
           'id_level' => 3,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which king met with the rebels of the Peasant\'s Revolt at Smithfield in 1381',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*42*/ Question::create([
           'id_level' => 5,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which castle in Gloucestershire is believed to be the scene of the murder of King Edward II in 1327',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*43*/ Question::create([
           'id_level' => 5,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'What was discovered in 1799 by Pierre-François Bouchard, a Napoleonic soldier',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*44*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Who was the last British monarch of the House of Hanover',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*45*/ Question::create([
           'id_level' => 2,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'What disease killed thousands of people in Glasgow in 1832',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*46*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which killer was also known as \'The Whitechapel Murderer\'',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*47*/ Question::create([
           'id_level' => 1,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'In which year was the Wall Street Crash',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*48*/ Question::create([
           'id_level' => 5,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'The Battle of Balaclava is a famous battle in which war',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*49*/ Question::create([
           'id_level' => 2,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'In which century was The Black Death',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);

          /*50*/ Question::create([
           'id_level' => 5,
           'id_category' => 4,
           'type_of_question' => 'multiOption',
           'question' => 'Which Portuguese-born navigator was the first European to cross the Pacific Ocean',
           'gems' => 15,
           'published' => true,
           'created_at' => $currentDate,
           'updated_at' => $currentDate
           ]);
    }
}
