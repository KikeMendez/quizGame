<?php

use Illuminate\Database\Seeder;
use App\User;
Use Carbon\Carbon;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->truncate();
      $currentDate = Carbon::now('America/Montevideo');

      User::create([
      'name' => 'leo messi',
      'email'=> 'messi@example.com',
      'password' => '$2y$10$8dsoHFpNoZV5TV3RGv14H.kh.Mf3bFZWGhUEr88.CIGLkAzPW6QmG',
      'id_level' =>1,
      'avatar' => 'https://qph.ec.quoracdn.net/main-thumb-t-18369-200-ldoorrbnefliolmgnzszesmuwtqgzpwy.jpeg',
      'remember_token' => 'zzb0v7JkRwaKLKoW32OB7bUsbayHKvMXBlo0Piowq8ZzyE8DOLETdIjKR1Pk',
      'created_at' =>	$currentDate,
      'updated_at' => $currentDate		
      ]);

      User::create([
      'name' => 'jhon doe',
      'email'=> 'jhondoe@example.com',
      'password' => '$2y$10$YUxColBXxYlbQG4V2oC5tO.t1Ac8LZBV97O3J3tbMi2zmA5AR02gO',
      'id_level' =>2,
      'avatar' => 'http://wpidiots.com/html/writic/red-writic-template/css/img/demo-images/avatar1.jpg',
      'remember_token' => '5Oxf9K55oUlqwIFMvzJ7YW3c0qJb1HkFdsUX8SEwYEjWKPD6UiV8h1l101VU',
      'created_at' =>	$currentDate,
      'updated_at' => $currentDate		
      ]);
    }
}
