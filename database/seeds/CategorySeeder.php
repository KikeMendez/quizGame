<?php

use Illuminate\Database\Seeder;

use App\Category;

Use Carbon\Carbon;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // truncate de table
     	DB::table('categories')->truncate();
     	// get the current day
      $currentDate	=	Carbon::now('America/Montevideo');

      /*1*/ Category::create([ 
      'name'	=>	'Sports',
      'description' =>	'How much do you know about sports? ',
      'avatar'	=>'http://www.lanchester.durham.sch.uk/wp-content/uploads/sites/28/2014/07/Sport-Clip-Art_200x200.jpg',
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*2*/ Category::create([
      'name'	=>	'Music',
      'description' =>	'How much do you know about music?',
      'avatar'	=>	'http://www.hingetownhoedown.com/wp-content/themes/adamos/images/music-icon.png',
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*3*/ Category::create([
      'name'	=>	'Movies',
      'description' =>	'How much do you know about movies?',
      'avatar'	=>	'http://www.quirindiroyaltheatre.com/wp-content/uploads/2015/11/movies-5.png',
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*4*/ Category::create([
      'name'	=>	'History',
      'description' =>	'How much do you know about history?',
      'avatar'	=>	'http://thorpepark.hull.sch.uk/images/mini-icons/curriculum/history.png',
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);

      /*5*/ Category::create([
      'name'  =>  'Geography',
      'description' =>  'How much do you know about geography?',
      'avatar'  =>  'http://thorpepark.hull.sch.uk/images/mini-icons/curriculum/geography.png',
      'created_at' => $currentDate,
      'updated_at' => $currentDate
      ]);
    }
}
