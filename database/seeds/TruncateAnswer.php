<?php

use Illuminate\Database\Seeder;

class TruncateAnswer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // @ Important
     	// Don't remove this truncate from here.
      DB::table('answers')->truncate();
    }
}
