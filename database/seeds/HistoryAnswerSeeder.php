<?php

use Illuminate\Database\Seeder;
use App\Answer;
use Carbon\Carbon;
class HistoryAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $currentDate = Carbon::now('America/Montevideo');

      /*1  When did the Cold War end */ 
            Answer::create([
            	'id_question' => 1,
            	'answer' => '1991',
            	'isCorrect' => 1,
            	'created_at' => $currentDate,
            	'updated_at' => $currentDate
            	]);

      /*1*/ Answer::create([
            	'id_question' => 1,
            	'answer' => '1988',
            	'isCorrect' => 0,
            	'created_at' => $currentDate,
            	'updated_at' => $currentDate
            	]);
            
      /*1*/ Answer::create([
            	'id_question' => 1,
            	'answer' => '1989',
            	'isCorrect' => 0,
            	'created_at' => $currentDate,
            	'updated_at' => $currentDate
            	]);

      /*2 When did Margaret Thatcher become Prime Minister */	
      			 Answer::create([
              'id_question' => 2,
              'answer' => '1979',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*2*/ Answer::create([
              'id_question' => 2,
              'answer' => '1974',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*2*/ Answer::create([
              'id_question' => 2,
              'answer' => '1976',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*3 Which famous battle between the British Royal Navy and the combined fleets of the French Navy and Spanish Navy took place on 21st October 1805 */ 
             Answer::create([
              'id_question' => 3,
              'answer' => 'Battle of Trafalgar',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*3*/ Answer::create([
              'id_question' => 3,
              'answer' => 'Battle of Waterloo',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*3*/ Answer::create([
              'id_question' => 3,
              'answer' => 'Battle of Trenton',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

        /*4 When was the euro introduced as legal currency on the world market */ 
             Answer::create([
              'id_question' => 4,
              'answer' => ' 1st January 1999',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*4*/ Answer::create([
              'id_question' => 4,
              'answer' => '1st January 1998',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*4*/ Answer::create([
              'id_question' => 4,
              'answer' => '1st January 1995',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*5 Who was Henry VIII first wife? */ 
             Answer::create([
              'id_question' => 5,
              'answer' => ' Catherine of Aragon',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*5*/ Answer::create([
              'id_question' => 5,
              'answer' => 'Anne of Cleves',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*5*/ Answer::create([
              'id_question' => 5,
              'answer' => 'Jane Seymour',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

       /*6 When was William Shakespeare born */ 
             Answer::create([
              'id_question' => 6,
              'answer' => ' 23rd April 1564',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*6*/ Answer::create([
              'id_question' => 6,
              'answer' => '23rd June 1564',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*6*/ Answer::create([
              'id_question' => 6,
              'answer' => '23rd May 1564',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*7 On what date did the Battle of Culloden take place */ 
             Answer::create([
              'id_question' => 7,
              'answer' => ' 16th April 1746',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*7*/ Answer::create([
              'id_question' => 7,
              'answer' => '23rd June 1674',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*7*/ Answer::create([
              'id_question' => 7,
              'answer' => '23rd May 1764',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*8 Who was the architect who designed the Millennium Dome? */ 
             Answer::create([
              'id_question' => 8,
              'answer' => 'Richard Rogers',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*8*/ Answer::create([
              'id_question' => 8,
              'answer' => 'Steve Rogers',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*8*/ Answer::create([
              'id_question' => 8,
              'answer' => 'Brian Rogers',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*9 When did the Eurostar train service between Britain and France start running? */ 
             Answer::create([
              'id_question' => 9,
              'answer' => '14th November 1994',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*9*/ Answer::create([
              'id_question' => 9,
              'answer' => '17th November 1996',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*9*/ Answer::create([
              'id_question' => 9,
              'answer' => '17th May 1994',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*10  How many wives did King Henry VIII have */ 
             Answer::create([
              'id_question' => 10,
              'answer' => 'Six',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*10*/ Answer::create([
              'id_question' => 10,
              'answer' => 'Twelve',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*10*/ Answer::create([
              'id_question' => 10,
              'answer' => 'Eight',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]); 

      /*11  What year was the Battle of Hastings */ 
             Answer::create([
              'id_question' => 11,
              'answer' => '1066',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*11*/ Answer::create([
              'id_question' => 11,
              'answer' => '1144',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*11*/ Answer::create([
              'id_question' => 11,
              'answer' => '1077',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*12 What was the name of the German leader during World War II */ 
             Answer::create([
              'id_question' => 12,
              'answer' => 'Adolf Hitler',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*12*/ Answer::create([
              'id_question' => 12,
              'answer' => 'Paul von Hindenburg',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*12*/ Answer::create([
              'id_question' => 12,
              'answer' => 'Karl Dönitz',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]); 

      /*13 In which year did the Victorian era begin */ 
             Answer::create([
              'id_question' => 13,
              'answer' => '1837',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*13*/ Answer::create([
              'id_question' => 13,
              'answer' => '1900',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*13*/ Answer::create([
              'id_question' => 13,
              'answer' => '1784',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]); 

      /*14 Robert Curthose, who died in Cardiff Castle in his early eighties, was the eldest son of which king */ 
             Answer::create([
              'id_question' => 14,
              'answer' => 'William I (William the Conqueror)',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*14*/ Answer::create([
              'id_question' => 14,
              'answer' => 'Edward IV',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*14*/ Answer::create([
              'id_question' => 14,
              'answer' => 'Richard III',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*15 Robert Curthose, who died in Cardiff Castle in his early eighties, was the eldest son of which king */ 
             Answer::create([
              'id_question' => 15,
              'answer' => 'Calais',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*15*/ Answer::create([
              'id_question' => 15,
              'answer' => 'Irland',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*15*/ Answer::create([
              'id_question' => 15,
              'answer' => 'Scotland',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*16 Which battle of 1571 marked the end of the Ottoman naval supremacy in the Mediterranean */ 
             Answer::create([
              'id_question' => 16,
              'answer' => 'The Battle of Lepanto',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*16*/ Answer::create([
              'id_question' => 16,
              'answer' => 'The Battle of Algiers',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*16*/ Answer::create([
              'id_question' => 16,
              'answer' => 'The Battle of Ludos',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]); 

      /*17 What tax was introduced in England and Wales in 1696 and repealed in 1851 */ 
             Answer::create([
              'id_question' => 17,
              'answer' => 'Window tax',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*17*/ Answer::create([
              'id_question' => 17,
              'answer' => 'Poor Law tax',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*17*/ Answer::create([
              'id_question' => 17,
              'answer' => 'Inheritance tax',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*18 which was the most famous battle of 1342? */ 
             Answer::create([
              'id_question' => 18,
              'answer' => 'The Battle of Crecy',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*18*/ Answer::create([
              'id_question' => 18,
              'answer' => 'The Battle of Waterloo',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*18*/ Answer::create([
              'id_question' => 18,
              'answer' => 'The Battle of Hasting',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*19 Which book was published in London on April 25th, 1719 */ 
             Answer::create([
              'id_question' => 19,
              'answer' => 'Robinson Crusoe',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*19*/ Answer::create([
              'id_question' => 19,
              'answer' => 'From the Earth to the Moon',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*19*/ Answer::create([
              'id_question' => 19,
              'answer' => 'Lord Of The Rings',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*20 The first day of which battle was the worst day in the history of the British Army, which suffered 60,000 casualties */ 
             Answer::create([
              'id_question' => 20,
              'answer' => 'Battle of the Somme',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*20*/ Answer::create([
              'id_question' => 20,
              'answer' => 'Battle of the Normandy',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*20*/ Answer::create([
              'id_question' => 20,
              'answer' => 'Battle of the Bulge',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*21 Who became president after the assassination of Abraham Lincoln */ 
             Answer::create([
              'id_question' => 21,
              'answer' => 'Andrew Johnson',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*21*/ Answer::create([
              'id_question' => 21,
              'answer' => ' Zachary Taylor',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*21*/ Answer::create([
              'id_question' => 21,
              'answer' => ' Ulysses S. Grant',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*22 Which city is normally accepted as being the ancient capital of Wessex */ 
             Answer::create([
              'id_question' => 22,
              'answer' => 'Winchester',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*22*/ Answer::create([
              'id_question' => 22,
              'answer' => 'Cambridge',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*22*/ Answer::create([
              'id_question' => 22,
              'answer' => 'Manchester',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*23 In which year was the death of Queen Elizabeth I */ 
             Answer::create([
              'id_question' => 23,
              'answer' => '1603',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*23*/ Answer::create([
              'id_question' => 23,
              'answer' => '1620',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*23*/ Answer::create([
              'id_question' => 23,
              'answer' => '1687',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*24 In 1297, at which battle did William Wallace defeat the English */ 
             Answer::create([
              'id_question' => 24,
              'answer' => 'Battle of Stirling Bridge',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*24*/ Answer::create([
              'id_question' => 24,
              'answer' => 'The Battle of Falkirk',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*24*/ Answer::create([
              'id_question' => 24,
              'answer' => 'The Battle of Stalling Down ',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*25 Name the second largest city in Britain during the Black Death */ 
             Answer::create([
              'id_question' => 25,
              'answer' => 'Bristol',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*25*/ Answer::create([
              'id_question' => 25,
              'answer' => 'Bath',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*25*/ Answer::create([
              'id_question' => 25,
              'answer' => 'London',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*26 In which war was The Battle of Agincourt */ 
             Answer::create([
              'id_question' => 26,
              'answer' => 'Hundred Years War',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*26*/ Answer::create([
              'id_question' => 26,
              'answer' => 'World War II',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*26*/ Answer::create([
              'id_question' => 26,
              'answer' => 'War of the Two Peters',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);


      /*27 What nationality was Karl Marx */ 
             Answer::create([
              'id_question' => 27,
              'answer' => 'German',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*27*/ Answer::create([
              'id_question' => 27,
              'answer' => 'Russian',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*27*/ Answer::create([
              'id_question' => 27,
              'answer' => 'English',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*28 Who commanded the British Expeditionary Force in World War One */ 
             Answer::create([
              'id_question' => 28,
              'answer' => 'Field Marshal Douglas Haig',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*28*/ Answer::create([
              'id_question' => 28,
              'answer' => ' General William Robertson',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*28*/ Answer::create([
              'id_question' => 28,
              'answer' => 'General Hubert Gough',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*29 Which country did Britain fight in the War of Jenkins\'s Ear */ 
             Answer::create([
              'id_question' => 29,
              'answer' => 'Spain',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*29*/ Answer::create([
              'id_question' => 29,
              'answer' => 'Scotland',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*29*/ Answer::create([
              'id_question' => 29,
              'answer' => 'Ireland',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*30 What was the largest naval battle of the First World War */ 
             Answer::create([
              'id_question' => 30,
              'answer' => 'Jutland',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*30*/ Answer::create([
              'id_question' => 30,
              'answer' => 'Heligoland Bight',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*30*/ Answer::create([
              'id_question' => 30,
              'answer' => 'Trafalgar',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*31 In which year was Abraham Lincoln assassinated */ 
             Answer::create([
              'id_question' => 31,
              'answer' => '1865',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*31*/ Answer::create([
              'id_question' => 31,
              'answer' => '1748',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*31*/ Answer::create([
              'id_question' => 31,
              'answer' => '1890',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*32 Name the second wife of Henry VIII */ 
             Answer::create([
              'id_question' => 32,
              'answer' => 'Anne Boleyn',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*32*/ Answer::create([
              'id_question' => 32,
              'answer' => 'Kathryn Howard',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*32*/ Answer::create([
              'id_question' => 32,
              'answer' => 'Jane Seymour',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*33 In which military campaign did Lewis Halliday and Basil Guy win Victoria Crosses in 1900 */ 
             Answer::create([
              'id_question' => 33,
              'answer' => 'The Boxer Rebellion',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*33*/ Answer::create([
              'id_question' => 33,
              'answer' => 'Uruguayan Civil War',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*33*/ Answer::create([
              'id_question' => 33,
              'answer' => 'Hundred Days',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*34 Who led a band of abolitionists that seized the federal arsenal at Harpers Ferry, Virginia, in October 1859 */ 
             Answer::create([
              'id_question' => 34,
              'answer' => 'John Brown',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*34*/ Answer::create([
              'id_question' => 34,
              'answer' => 'James Brown',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*34*/ Answer::create([
              'id_question' => 34,
              'answer' => 'Charles Brown',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*35 Which English philosopher, regarded as one of the most influential of Enlightenment thinkers, was known as the Father of Classical Liberalism */ 
             Answer::create([
              'id_question' => 35,
              'answer' => 'John Locke',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*35*/ Answer::create([
              'id_question' => 35,
              'answer' => 'Thomas Hobbes',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*35*/ Answer::create([
              'id_question' => 35,
              'answer' => 'Jeremy Bentham',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);


      /*36 Which German city endured the worst bombing of World War II in February 1945 */ 
             Answer::create([
              'id_question' => 36,
              'answer' => 'Hamburg',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*36*/ Answer::create([
              'id_question' => 36,
              'answer' => 'Dresden',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*36*/ Answer::create([
              'id_question' => 36,
              'answer' => 'Berlin',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*37 Robert Curthose was the son of which king */ 
             Answer::create([
              'id_question' => 37,
              'answer' => 'William the Conqueror',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*37*/ Answer::create([
              'id_question' => 37,
              'answer' => 'Henry VIII',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*37*/ Answer::create([
              'id_question' => 37,
              'answer' => 'Henry I',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*38 Who was king before Queen Victoria */ 
             Answer::create([
              'id_question' => 38,
              'answer' => 'William IV',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*38*/ Answer::create([
              'id_question' => 38,
              'answer' => 'EDWARD VII',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*38*/ Answer::create([
              'id_question' => 38,
              'answer' => 'George V',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*39 Which Empire declared war on Russia in October 1853, suffering a defeat that gave Russia control of the Black Sea */ 
             Answer::create([
              'id_question' => 39,
              'answer' => 'The Ottoman Empire',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*39*/ Answer::create([
              'id_question' => 39,
              'answer' => 'The British Empire',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*39*/ Answer::create([
              'id_question' => 39,
              'answer' => 'The Spanish Empire',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*40 Which Roman road stretched 220 miles from Exeter to Lincoln */ 
             Answer::create([
              'id_question' => 40,
              'answer' => 'Fosse Way',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*40*/ Answer::create([
              'id_question' => 40,
              'answer' => 'Portway',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*40*/ Answer::create([
              'id_question' => 40,
              'answer' => 'Dere Street',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*41 Which king met with the rebels of the Peasant\'s Revolt at Smithfield in 1381 */ 
             Answer::create([
              'id_question' => 41,
              'answer' => 'Richard II',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*41*/ Answer::create([
              'id_question' => 41,
              'answer' => 'William the Conqueror',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*41*/ Answer::create([
              'id_question' => 41,
              'answer' => 'Edward VI',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*42 Which castle in Gloucestershire is believed to be the scene of the murder of King Edward II in 1327 */ 
             Answer::create([
              'id_question' => 42,
              'answer' => 'Berkeley castle',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*42*/ Answer::create([
              'id_question' => 42,
              'answer' => 'Brimpsfield Castle',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*42*/ Answer::create([
              'id_question' => 42,
              'answer' => 'Castle Hale',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*43 What was discovered in 1799 by Pierre-François Bouchard, a Napoleonic soldier */ 
             Answer::create([
              'id_question' => 43,
              'answer' => 'The Rosetta Stone',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*43*/ Answer::create([
              'id_question' => 43,
              'answer' => 'The Sword In The Stone',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*43*/ Answer::create([
              'id_question' => 43,
              'answer' => 'Lascaux Cave',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*44 Who was the last British monarch of the House of Hanover */ 
             Answer::create([
              'id_question' => 44,
              'answer' => 'Queen Victoria',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*44*/ Answer::create([
              'id_question' => 44,
              'answer' => 'Mary II',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*44*/ Answer::create([
              'id_question' => 44,
              'answer' => 'Queen Elizabeth II',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*45 What disease killed thousands of people in Glasgow in 1832 */ 
             Answer::create([
              'id_question' => 45,
              'answer' => 'Cholera',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*45*/ Answer::create([
              'id_question' => 45,
              'answer' => 'Yellow Fever',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*45*/ Answer::create([
              'id_question' => 45,
              'answer' => 'Bubonic Plague',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*46 Which killer was also known as 'The Whitechapel Murderer' */ 
             Answer::create([
              'id_question' => 46,
              'answer' => 'Jack the Ripper',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*46*/ Answer::create([
              'id_question' => 46,
              'answer' => 'Anthony Hardy',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*46*/ Answer::create([
              'id_question' => 46,
              'answer' => 'Levi Bellfield',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*47 In which year was the Wall Street Crash */ 
             Answer::create([
              'id_question' => 47,
              'answer' => '29th Oct, 1929',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*47*/ Answer::create([
              'id_question' => 47,
              'answer' => '24th Nov, 1945',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*47*/ Answer::create([
              'id_question' => 47,
              'answer' => '21st Nov, 1946',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*48 The Battle of Balaclava is a famous battle in which war */ 
             Answer::create([
              'id_question' => 48,
              'answer' => 'The Crimean War',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*48*/ Answer::create([
              'id_question' => 48,
              'answer' => 'The Hundred Years War',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*48*/ Answer::create([
              'id_question' => 48,
              'answer' => 'World War I',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*49 In which century was The Black Death */ 
             Answer::create([
              'id_question' => 49,
              'answer' => ' 14th century',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*49*/ Answer::create([
              'id_question' => 49,
              'answer' => ' 17th century',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*49*/ Answer::create([
              'id_question' => 49,
              'answer' => ' 15th century',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*50 Which Portuguese-born navigator was the first European to cross the Pacific Ocean */ 
             Answer::create([
              'id_question' => 50,
              'answer' => 'Ferdinand Magellan',
              'isCorrect' => 1,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*50*/ Answer::create([
              'id_question' => 50,
              'answer' => 'Vasco da Gama ',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

      /*50*/ Answer::create([
              'id_question' => 50,
              'answer' => 'Pedro Alvares Cabral',
              'isCorrect' => 0,
              'created_at' => $currentDate,
              'updated_at' => $currentDate
              ]);

    }
}
