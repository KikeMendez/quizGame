<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegisterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    /** @Test **/
    public function testRegister()
    {
        $this->visit('/register')
     		->type('messi', 'name')
            ->type('messi@example.com', 'email')
            ->type('hello1', 'password')
            ->type('hello1', 'password_confirmation')
     		->press('Register')
     		->seePageIs('/register')
     		->assertTrue(true);
    }
}
