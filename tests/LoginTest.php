<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLogin()
    {
    	  $this->visit('/login')
          ->type('messi@example.org', 'email')
          ->type('123123', 'password')
          ->check('remember')
          ->press('Login')
          ->seePageIs('/login');
    }

    public function testPasswordResetLink(){
    		$this->visit('login')
    			->click("Forgot Your Password?");
    }
}
