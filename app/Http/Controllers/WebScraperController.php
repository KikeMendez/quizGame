<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Goutte\Client; // scrapper class

class WebScraperController extends Controller
{
  // url
  private $url = 'http://www.freepubquiz.co.uk/sport-quiz-questions.html';

	public function index(){

			//  Create a new Goutte client instance
	   	$client = new Client();

	   	// making the request
	   	$crawler = $client->request('GET', $this->url);
	 		
	 		// passing the filters
	 		$crawler->filter('li')->each(function ($node) {
	 		    print $node->text()."</br>\n";
	 		});
	}

}
