<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Card;
use Auth;
use Crypt;

class ShopController extends Controller
{	
	/**
	* Display the shop it-self
	*/ 
   public function index(){
   		$cards = Card::getallCards();	
   		return view('shop.shop',compact('cards'));
   }

   /**
   *	Process the shop request
   */ 
   public function buyCard(Request $request)
   {	
		 //dd($request->all());
		$card = Crypt::decrypt($request->input('card'));

		switch ($card) {
			case '1': // Wizard
				$wizard = Card::find($card);
				$userGems = Auth::user()->getUserKit->gems;
				if($userGems < $wizard->price){
					return $this->errorNotEnoughMoney();
				}
				Auth::user()->BuyWizardCard($wizard->price);
				return back();
			break;

			case '2': // Next Question
				$nextQuestion = Card::find($card);
				$userGems = Auth::user()->getUserKit->gems;
				if($userGems < $nextQuestion->price){
					return $this->errorNotEnoughMoney();
				}
				Auth::user()->BuyNextQuestion($nextQuestion->price);
				return back();
			break;
			
			default: // Life
				$life = Card::find($card);
				$userGems = Auth::user()->getUserKit->gems;
				if($userGems < $life->price){
					return $this->errorNotEnoughMoney();
				}
				Auth::user()->Buylife($life->price);
				return back();
			break;
		}
   } // end of buy card method

   // Error message shown when the user don't have enought gems to buy
   private function errorNotEnoughMoney(){
      return "Ups.. It's seems you don't have enought gems to buy this card";
   }
}
