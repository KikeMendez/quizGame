<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Crypt;
use App\Http\Requests;
use App\User;
use App\Question;
use App\Answer;
use Carbon\Carbon;

class GameController extends Controller
{
   public function __construct(){
        $this->middleware('auth');
   }

   public function play(Request $request){
     	//	Validate the requested form
     	$this->validate($request, [
     		'category' => 'required',   	    
     	]);
      /**
      * Here I will decrypt the category id taken from the form
      */  
      $category = Crypt::decrypt($request->category);
      /**
      * First find the current user 
      */
      $user = User::find(Auth::user()->id);
      $level = $user->id_level;
      /*  Questions already aswered correctly*/
      $questionsIdOfRightAnswers = $user->getUserArchiveOfRightAnswer();
      
      /**
      * Get a question to play and the possibles answers.
      * 
      */
    	$instance = new Question();
    	$question = $instance->getQuestionByCategoryAndByLevel($category,$level,$questionsIdOfRightAnswers );
      // If there not a question available for this user THINK WHAT NEXT 
      if ($question == false) {
        return "You have no more questions to play in this category".$category;
      }

    	$answers  = $question->getPossibleAnswers();
    	/**
    	*	The next switch checks what kind of question was taken
    	*	Then return the right view for that question 
    	*/
    	switch ($question->type_of_question) {
    		case 'multiOption':
 				return view('game/multiOptions',compact('question','answers')) ;
 			break;
    		
    		case 'trueOrFalse':
 				return view('game/trueOrFalse',compact('question','answers')) ;
 			break;

    		default:
    			# code...
    		break;
    	}     
   }
  
   /**
   *	First Validate the requested form
   *	Next decrypt the values taken from the form
   *	Next find the correct answer
   *	Then check if the answer choosen for the user is correct one
   */
   public function checkMultiOptionsAnswer(Request $request){
 		// Validate the requested form
 		$this->validate($request, [
 			'answer' => 'required',
 			'question'	=>	'required'   	    
 		]);
 		/**
 		*	Here I will decrypt the values taken from the form
 		*	Values: question & answer
 		*/	
      $question_id = Crypt::decrypt($request->question);
      $userAnswer  = Crypt::decrypt($request->answer);
    	// Find the question	and the correct answer
    	$currentQuestion = Question::find($question_id);
    	if($currentQuestion){
    		$answers =	$currentQuestion->getCorrectAnswer();
    	}
      /**
      *	Check if the answers matched
      *	If so, 
      *	Add up and save the prizes earned for the user 
      */
      if ($userAnswer === $answers->isCorrect) 
     	{
     		$user = User::find(Auth::user()->id);
        // save in user_question_archive
        $user->setUserQuestionArchive($currentQuestion->id, $isCorrect = true);
     		$user->updateGems($currentQuestion->gems);
     		return redirect('home');
     		
     	}else {
     		// Think what would happens if the answer is wrong
        $user = User::find(Auth::user()->id);
        $user->setUserQuestionArchive($currentQuestion->id, $isCorrect = false);
     		return "Wrong Answer.. maybe next time";
     	}		
	} 	
}