<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;

class CategoryController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function index(){
    	$categories = Category::all();
    	return view('game.categories',compact('categories'));
    }
}
