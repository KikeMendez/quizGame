<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Level extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    // Make the relationship between levels and the users
    // so now we can fetch all user who has the same level
    public function user(){
        return $this->hasMany(User::class,'id_level');
    }

    public function question(){
        return $this->hasMany(Question::class,'id_level');
    }
}
