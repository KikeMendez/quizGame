<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\WelcomeToMywebsite;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Find an user seraching by email
    public static function findByEmail($email)
    {    
        return static::where('email', $email)->first();
    }

    // Notify the user by email.
    public function send_welcome_email($user) {       
        $this->notify(new WelcomeToMywebsite($user));    
    } 

    // Make the relationship between users and levels 
    // So now We can get the current user level and levels table properties
    public function getLevel(){
       return $this->belongsTo(Level::class,'id_level');
    }
    /**
    * The next method make a relationship between
    *  Users and UserQuestionArchive
    */
    public function getUserQuestionArchive(){
      return $this->hasMany(UserQuestionArchive::class);
    } 

    /**
    * The next method make a relationship between
    *  Users and UserKits
    */
    public function getUserKit(){
      return $this->hasOne(UsersKit::class);
    } 

    // This method set the first user kit after registration.
    public function setUserKit(){
        return $this->getUserKit()->firstOrCreate([
          'life' =>5,
          'gems' => 1500, 
          'wizard'=> 1,
          'nextQuestion' =>1 
          ]);
    }


   // This method convert the date format to a nice format like this..
   // Dec 12, 2016
   // IMPORTANT. To access to this method try it on this way 
   // Auth::user()->convertDateToString();
   public function convertDateToString(){
        return  $this->created_at->toFormattedDateString(); 
   }
   /**
   *  This method update the user gems in the user kit
   */
   public function updateGems($gems){
      if($gems){
        $this->getUserKit->gems = $this->getUserKit->gems + $gems;
        return $this->getUserKit->save();   
      }else{
        return false;
      }
   }

   /**
   *  Get user's archive
   *  Then get all question's id  which were answered correctly
   *  
   */
   public function getUserArchiveOfRightAnswer(){
      $results = $this->getUserQuestionArchive()->where('isCorrect', 1)->get();
      
      if($results->isEmpty()){
           return $rightAnswerQuestion_id = [];} 
      else{
        foreach ($results as $result) {
          $rightAnswerQuestion_id[] = $result->question_id;    
        }
        return $rightAnswerQuestion_id;
      }
    }

    public function setUserQuestionArchive($question_id,$isCorrect){
      return $this->getUserQuestionArchive()
                  ->create([
                      'question_id'=>$question_id,
                      'isCorrect'=>$isCorrect
                  ]);
    }

    // Count all right answers that an user have on his history
    public function countAllRightAnswers(){
      return $this->getUserQuestionArchive()->where('isCorrect',true)->count();
    }

    // Count all wrong answers that an user have on his history
    public function countAllWrongAnswers(){
      return $this->getUserQuestionArchive()->where('isCorrect',false)->count();
    }
    
    /**
    * Update User kit
    */
    //  Add a wizard
    public function BuyWizardCard($gems){
      $updatedGems = $this->getUserKit->gems - $gems;
      $wizard = $this->getUserKit->wizard + 1;
      return $this->getUserKit->update(['wizard'=> $wizard, 'gems' => $updatedGems]);
    }
    //  Add a nextQuestion
    public function BuyNextQuestion($gems){
      $updatedGems = $this->getUserKit->gems - $gems;
      $nextQuestion = $this->getUserKit->nextQuestion + 1;
      return $this->getUserKit->update(['nextQuestion'=> $nextQuestion, 'gems' => $updatedGems]);
    }
    // Add a life
    public function Buylife($gems){
      $updatedGems = $this->getUserKit->gems - $gems;
      $life = $this->getUserKit->life + 1;
      return $this->getUserKit->update(['life'=> $life, 'gems' => $updatedGems]);
    }
}// end of class