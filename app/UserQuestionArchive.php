<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuestionArchive extends Model
{

	protected $fillable = [
	    'question_id', 'isCorrect',
	];
  /**
  *	Make the relationship between user_question_archives and users tables
  */
	public function getUser(){
		return $this->belongsTo(User::class);
	}

}
