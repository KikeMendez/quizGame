<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    // Make the relationship 
    // Any answer belongs to just to one single question.
    public function question(){
        return $this->belongsTo(Question::class,'id');
    }

    public function category(){
        return $this->belongsTo(Category::class,'id');
    }
   
}
