<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersKit extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'wizard','gems','nextQuestion','life'   
    ];

    public function getUser(){
    return $this->belongsTo(User::class);
    }

}
