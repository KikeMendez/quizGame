<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
  	/**
    * Make the relationship betweend Questions and Answers
    * A question can have many answers
    */
    public function answers(){
	   	return $this->hasMany(Answer::class,'id_question');
    }
    /**
    * Make the relationship between Questions And Categories
    * A question belongs to a category
    */
    public function category(){
    	return $this->belongsTo(Category::class,'id_category');
    }
    /**
    * Make the relationship between Question And Levels
    * A question belongs to a level
    */
    public function level(){
    	return $this->belongsTo(Level::class,'id_level');
    }
    /**
	  *  This method gets all questions in random order where the parameters given matched
	  *  Then return the first question.		
    */
    public function getQuestionByCategoryAndByLevel($category,$gameUserLevel,$Question_idOfCorrectlyAnswered){
      $question = $this->whereNotIn('id',$Question_idOfCorrectlyAnswered)
                  ->where('id_category',$category)
                  ->where('id_level','<=',$gameUserLevel)
                  ->where('published',1)
                  ->inRandomOrder()
                  ->get()->first(); 

      if (empty($question)) {
        return false;
      }else{
        return $question;
      }
   }
   /**
   *    Get all possible answers for a single question
   *    Then sort them in a random order.
   */
   public function getPossibleAnswers(){
        return $this->answers()->inRandomOrder()->get();
   }
   /**
   *    Get Only the correct answer for a single question
   */
   public function getCorrectAnswer(){
       return $this->answers()->where('isCorrect',1)->get()->first();
   }

}// end of the class
