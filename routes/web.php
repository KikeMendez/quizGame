<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Http\Request;
Route::get('/', function () { 
	return view('welcome'); 		
});

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('/robo', 'WebScraperController@index');

// Game Classic Routes
Route::get('/categories','CategoryController@index');
Route::post('/play/category/{category}','GameController@play');
Route::post('/answer','GameController@checkMultiOptionsAnswer');

// Game Battle Routes
Route::get('/battle','BattleController@index');

//	Shop Routes
Route::get('/shop','ShopController@index');
Route::post('/buy','ShopController@buyCard');

// Ranking Routes
Route::get('/ranking',function(){
	return "The ranking ";
});	
// TESTING ROUTE
Route::get('/test', function(){
	return "testing area";
});

