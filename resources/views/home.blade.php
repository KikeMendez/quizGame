@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4>
                        <img class="img-responsive img-circle picture_profile" 
                            src="{{ Auth::user()->avatar }}" 
                            alt="{{ Auth::user()->name }}">
                        {{ Auth::user()->name}}
                    </h4>
                        <p>Joined on: {{ Auth::user()->convertDateToString() }}</p>
                </div>
                {{-- panel body --}}
                <div class="panel-body game_profile">
                    <strong>Ranking Position:</strong>  
                    <p class="user_gems">1245</p>
                </div>

                <div class="panel-body game_profile">
                    <strong>Level:</strong>  
                    <p class="user_gems">{{ Auth::user()->getLevel->name }}</p>
                </div>
                 
                <div class="panel-body game_profile">
                    <strong>Lifes:</strong>  
                    <p class="user_gems">4</p>
                </div>

                <div class="panel-body game_profile">
                    <strong>Gems:</strong>  
                    <p class="user_gems">{{ Auth::user()->getUserKit->gems }}</p>
                </div>

                <div class="panel-body game_profile">
                    <strong>Wizards:</strong>  
                    <p class="user_gems">{{ Auth::user()->getUserKit->wizard }}</p>
                </div>

                <div class="panel-body game_profile">
                    <strong>Next one:</strong>  
                    <p class="user_gems">{{ Auth::user()->getUserKit->nextQuestion }}</p>
                </div>

                 <div class="panel-body game_profile">
                    <strong>Wrong Answers:</strong>  
                    <p class="user_gems"> {{ Auth::user()->countAllWrongAnswers()  }} </p>
                </div>

                <div class="panel-body game_profile">
                    <strong>Correct Answers:</strong>  
                    <p class="user_gems">{{ Auth::user()->countAllRightAnswers() }} </p>
                </div>

            </div>    
        </div>
        {{-- game options --}}
         <div class="col-md-4">
               <div class="thumbnail">
                 <img src="https://cdn4.iconfinder.com/data/icons/squared-line-generic-2/64/shop-building-ecommerce-store-128.png" alt="...">
                 <div class="caption">
                   <h3 style="text-align:center;">Shop</h3>
                   <p style="text-align:center;">{{ Auth::user()->name  }} get some extra weapons!!</p>
                   <p><a href="{{url('shop')}}" class="btn btn-danger btn-group-justified" role="button">Shop</a></p>
                 </div>
               </div>
         </div>

          <div class="col-md-5">
            <div class="thumbnail">
              <img src="https://cdn2.iconfinder.com/data/icons/thin-line-icons-for-seo-and-development-1/64/SEO_stopwatch_timer_performance-128.png" alt="...">
              <div class="caption">
                <h3 style="text-align:center;">Classic Mode</h3>
                <p style="text-align:center;">{{ Auth::user()->name  }} chose one category and start to play!!</p>
                <p><a href="{{url('categories')}}" class="btn btn-success btn-group-justified" role="button">Play</a></p>
              </div>
            </div>
          </div>


        <div class="col-md-5">
            <div class="thumbnail">
              <img src="http://previewcf.turbosquid.com/Preview/2014/07/11__15_44_46/medievalshield_01_01.jpge1165a98-4f1c-47ea-ae25-41d00b26d1a9Medium.jpg" alt="...">
              <div class="caption">
                <h3 style="text-align:center;">Battle</h3>
                <p style="text-align:center;">{{ Auth::user()->name  }} play against a random player!!</p>
                <p><a href="{{url('battle')}}" class="btn btn-success btn-group-justified" role="button">Play</a></p>
              </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
              <img src="http://fextralife.com/wp-content/uploads/2013/08/Gold-trophy.png" alt="...">
              <div class="caption">
                <h3 style="text-align:center;">Ranking</h3>
                <p style="text-align:center;">{{ Auth::user()->name  }} this is the ranking so far</p>
                <p><a href="{{url('ranking')}}" class="btn btn-primary btn-group-justified" role="button">Look</a></p>
              </div>
            </div>    
        </div>

    </div> <!--End of row-->

</div> <!--End of container-->
@endsection
