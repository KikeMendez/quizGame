@extends('layouts/app')
@section('content') 
<div class="container">                         
	<div class="row">
		@foreach ($categories as $category)
			<form method="POST" action="/play/category/{{ strtolower($category->name) }}">
					<div class="col-sm-6 col-md-3 ">
					  <div class="thumbnail">
					    <img src="{{ $category->avatar }}" alt="{{ $category->name }}">
					    <div class="caption">
					      <h3>{{ $category->name }}</h3>
					      <p>{{ $category->description }}</p>
					     {{ csrf_field() }}
					     <button name="category" type="submit" class="btn btn-primary btn-lg btn-group-justified"  
					         value="{{ Crypt::encrypt($category->id)}}">{{ $category->name }}
					     </button>  
					    </div>
					  </div>
					</div>
			</form> 
		@endforeach
	</div>
</div>
@stop
 
                      
                                                                       
                             
                  