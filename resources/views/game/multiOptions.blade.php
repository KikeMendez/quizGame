@extends('layouts/app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-12">
					<div class="jumbotron">
						<div class="page-header">
						  <p>{{ $question->question }} ? <small>10 gems</small></p>
						</div>
					 
					  <div class="panel-body">
						  <div class="container">
						  		<form action="/answer" method="post">
						  			{{ csrf_field() }}
						  				<input type="hidden" name="question" value="{{ Crypt::encrypt($question->id) }}">
						  				@foreach ($answers as $answer)
						  					<button name="answer" class="btn btn-primary btn-lg" 
						  							value="{{ Crypt::encrypt($answer->isCorrect) }}" type="submit">
						  							{{ $answer->answer }}
						  					</button>
						  				@endforeach
						  		</form>
						  	</div>
					  </div>
					</div>
				</div>
		</div>
	</div>
@stop

