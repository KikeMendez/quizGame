<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Quizz application">
    <meta name="keywords" content="Questions,Answers,Game,Trivia">
    <meta name="author" content="Kike Mendez">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- Main style's file--}} 
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    {{--  We include the main navigation menu. --}}
    @include('layouts/nav')

    {{-- Main content for the website will be in the yield content. --}}
    @yield('content')

    {{-- Main script's file --}}
    <script src="/js/app.js"></script>
</body>
</html>
