@extends('layouts/app')
@section('content')
	<h3 style="text-align:center; margin:auto;"><strong>{{ ucwords(Auth::user()->name)  }}'s kit</strong></h3>
	<div class="container">
		<hr>
		<div class="row" style="margin-top:0px;">
			<div class="col-md-3" style="text-align:center;">
				<div class="thumbnail" style="padding:1px;">
					<h3>	Wizards
						<img src="https://lh3.googleusercontent.com/-qLRFzGXbsic/AAAAAAAAAAI/AAAAAAAAAGo/gDpAJCKiW9o/s46-c-k-no/photo.jpg" alt="">
						X {{ Auth::user()->getUserKit->wizard }}</h3>
				</div>
			</div>

			<div class="col-md-3" style="text-align:center;">
				<div class="thumbnail" style="padding:1px;">
					<h3>	Next Ones
						<img src="http://do2learn.com/JobTIPS/images/btn-next.png" alt="">
						X {{ Auth::user()->getUserKit->nextQuestion }}</h3>
				</div>
			</div>

			<div class="col-md-3" style="text-align:center;">
				<div class="thumbnail" style="padding:1px;">
					<h3>	Lifes
						<img src="https://lh3.googleusercontent.com/-fkxH_FHPfQ0/AAAAAAAAAAI/AAAAAAAABbc/V1GyEGhvyyE/s46-c-k-no/photo.jpg" alt="">
						X {{ Auth::user()->getUserKit->life }}</h3>
				</div>
			</div>

			<div class="col-md-3" style="text-align:center;">
				<div class="thumbnail" style="padding:1px;">
					<h3> Gems
						<img src="https://lh3.googleusercontent.com/-qa06_kYrqqo/AAAAAAAAAAI/AAAAAAAAADU/247ncobcOLA/s46-c-k-no/photo.jpg" alt="">
						X {{ Auth::user()->getUserKit->gems }}</h3>
				</div>
			</div>
	</div>
		<h3 style="text-align:center; margin:auto; padding:10px;"><strong>Add New Cards To Your Kit</strong></h3>
	<div class="row">
		@foreach ($cards as $card)
				<form action="{{ url('/buy') }}" method="POST" name="shop">
						{{ csrf_field() }}
						<div class="col-md-3">
							<div class="thumbnail">
								<img src="{{ $card->picture }}" alt="{{ $card->name}}" 
									class="img-responsive img-thumbnail">
									<div class="caption">
										<h3>{{ $card->name }}</h3>
										<p>{{ $card->description }}</p>
									</div>
									<button type="submit" name="card" value="{{ Crypt::encrypt($card->id) }}"  class="btn btn-success btn-lg btn-group-justified"><img src="https://lh3.googleusercontent.com/-qa06_kYrqqo/AAAAAAAAAAI/AAAAAAAAADU/247ncobcOLA/s46-c-k-no/photo.jpg" alt="">x {{ $card->price }}</button>  
							</div>
						</div>
				</form>
			@endforeach
	</div>				
@stop
